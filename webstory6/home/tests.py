from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
from .views import index
from .models import inputStatus
from .views import tampilprofil

# Create your tests here.
class Story6UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_is_exist(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		new_status = inputStatus.objects.create(status='mengerjakan lab ppw')
		counting_all_status = inputStatus.objects.all().count()
		self.assertEqual(counting_all_status, 1)

	def test_story6_using_satu_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'satu.html')

	def test_story6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/input-data', {'status': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_story6_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().get('/input-data', {'status': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_challange6_url_exist(self):
		response = Client().get('/profil')
		self.assertEqual(response.status_code, 200)

	def test_challange6_tampilprofil_is_exist(self) :
		found = resolve('/profil')
		self.assertEqual(found.func, tampilprofil)

	def test_challange6_cekHTML(self) :
		response = Client().get('/profil')
		self.assertTemplateUsed(response, 'profil.html')