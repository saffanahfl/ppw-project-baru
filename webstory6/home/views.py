from django.shortcuts import render
from .models import inputStatus
from .forms import input_form
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def index(request) :
	response['status_form'] = input_form
	response['data'] = inputStatus.objects.all()
	html = 'satu.html'
	return render(request, html, response)

def masukin_data(request) :
	form = input_form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		stat = inputStatus(status=response['status'])
		stat.save()
		return HttpResponseRedirect('../')
	else:
		return HttpResponseRedirect('../')

def tampilprofil(request):
	return render(request, 'profil.html')