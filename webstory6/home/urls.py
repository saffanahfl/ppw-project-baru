from django.conf.urls import url
from .views import index
from .views import masukin_data
from .views import tampilprofil
urlpatterns = [
	url(r'^$', index, name = 'index'),
	url(r'^input-data', masukin_data, name = 'masukin_data'),
	url(r'^profil', tampilprofil, name = 'tampilprofil'),
]

